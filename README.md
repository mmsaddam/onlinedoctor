##Online Doctor 
========================

[![Platform](http://img.shields.io/badge/platform-ios-blue.svg?style=flat
)](https://developer.apple.com/iphone/index.action)
[![License](http://img.shields.io/badge/license-MIT-lightgrey.svg?style=flat
)](http://mit-license.org)

![sample](screenshots/img1.png)


##Language

Objective-C

##Tools
Xcode 6.3, Cocoa Touch

##Requirement
iOS 7
Only for iPhone 5, iPod Touch 5G

 

##About The App
This is online treatment app management for iOS. After sign in the app user can see available doctors whose are now online. 
Then he can discuss with the doctor about his problem. He can also send this soft-copy of his previous report by scanning at the real time using the app.
User also can see his previous history and also can get the schedule of the doctor for future discussion.

## Contributing

Forks, patches and other feedback are welcome.

## Creator

[Muzahidul Islam](http://mmsaddam.github.io/) 
[Blog](http://mmsaddam.github.io/)

## License

Online Doctor is available under the MIT license. See the LICENSE file for more info.