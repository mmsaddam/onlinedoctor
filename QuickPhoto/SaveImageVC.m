//
//  SaveImageVC.m
//  Flowbook
//


#import "SaveImageVC.h"
#import "AppDelegate.h"

#import "NSString+Extensions.h"
#import "DDExpandableButton.h"
#define radians( degrees ) ( degrees * M_PI / 180 )

@implementation SaveImageVC
@synthesize frameBtn;
@synthesize flashBtn;
@synthesize arrowBtn;
@synthesize retakeBtn;
@synthesize closeBtn;
@synthesize tickBtn;
@synthesize cameraBtn;
@synthesize cameraImage;
@synthesize isFromShare;
@synthesize delegate;
@synthesize pictureImage;
@synthesize btnDone;
@synthesize lblPreview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil image:(UIImage *)cameraimage
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.pictureImage = cameraimage;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    cameraImage.image = pictureImage;
    isFromShare = TRUE;
    if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    cameraImage.frame = CGRectMake(0, 0, 320, self.view.frame.size.height - 56);
    
    DDExpandableButton *qualityBtn = nil;
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    if([str isEqualToString:@"en"]){
        qualityBtn = [[[DDExpandableButton alloc] initWithPoint:CGPointMake(10.0,7.0)
																		   leftTitle:nil
																			 buttons:[NSArray arrayWithObjects:@"Normal", @"Good", @"Excellent", nil]] autorelease];
    }
    else{
        qualityBtn = [[[DDExpandableButton alloc] initWithPoint:CGPointMake(10.0,7.0)
                                                      leftTitle:nil
                                                        buttons:[NSArray arrayWithObjects:@"Normal", @"Bon", @"Excellent", nil]] autorelease];
    }
	[[self view] addSubview:qualityBtn];
	[qualityBtn addTarget:self action:@selector(qualityBtnAction:) forControlEvents:UIControlEventValueChanged];
	[qualityBtn setVerticalPadding:6];
	[qualityBtn updateDisplay];
    
	[qualityBtn setSelectedItem:[[NSUserDefaults standardUserDefaults] integerForKey:@"imagequality"]];
    [self changeStuffAsPerLanguage];
}
-(void)changeStuffAsPerLanguage{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    if([str isEqualToString:@"en"]){
        lblPreview.text = @"Preview";
        [btnDone setTitle:@"Done" forState:UIControlStateNormal];
        [retakeBtn setTitle:@"Retake" forState:UIControlStateNormal];

        
    }
    else if([str isEqualToString:@"fr"]){
        lblPreview.text =@"aperçu";
        [btnDone setTitle:@"Utiliser" forState:UIControlStateNormal];
        [retakeBtn setTitle:@"Reprendre" forState:UIControlStateNormal];

    }
}
-(void)qualityBtnAction:(id)sender{
    [[NSUserDefaults standardUserDefaults] setInteger:[sender selectedItem] forKey:@"imagequality"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(IBAction)retakeBtnAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    isFromShare = FALSE;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}


-(IBAction)saveBtnAction:(id)sender{
    isFromShare = FALSE;
    UIImage *image = nil;
    NSInteger iQuality = [[NSUserDefaults standardUserDefaults] integerForKey:@"imagequality"];
    if(iQuality == 0){
        self.pictureImage = [self portraitImageForImage:pictureImage];
        image = [UIImage imageWithData:UIImageJPEGRepresentation(pictureImage, 0.0)];
    }
    else if(iQuality ==1){
        image = [UIImage imageWithData:UIImageJPEGRepresentation(pictureImage, 0.6)];
    }
    else{
        image= [UIImage imageWithData:UIImageJPEGRepresentation(pictureImage, 0.9)];
    }
    if([delegate respondsToSelector:@selector(customImagePickerController:didFinishPickingMediaWithInfo:)]){
        [delegate customImagePickerController:nil didFinishPickingMediaWithInfo:[NSMutableDictionary dictionaryWithObject:image forKey:@"UIImagePickerControllerOriginalImage"]];
    }
    [self dismissModalViewControllerAnimated:YES];

}

- (UIImage *)portraitImageForImage:(UIImage *)image
{
    int kMaxResolution = 1500; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
//    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
//    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
  /*  switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }*/
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    

    
    return imageCopy;
    
    //    return image;
    
    //    return [UIImage imageWithCGImage:[image CGImage] scale:1.0 orientation:UIImageOrientationUp];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)dealloc{
    self.pictureImage = nil;
    self.delegate = nil;
    self.frameBtn = nil;
    self.flashBtn = nil;
    self.arrowBtn = nil;
    self.retakeBtn = nil;
    self.closeBtn = nil;
    self.cameraBtn = nil;
    self.cameraImage = nil;
    self.delegate = nil;
    self.btnDone = nil;
    self.lblPreview = nil;
    [super dealloc];
}


@end
