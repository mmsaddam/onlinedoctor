

#import <UIKit/UIKit.h>
#import "PullRefresh_PagingViewController.h"
#import "CustomImagePickerDelegate.h"


@interface PatientListVC : PullRefresh_PagingViewController<UITableViewDelegate,UITableViewDataSource ,WebserviceDelegate,UISearchBarDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate,CustomImagePickerDelegate >{
    NSMutableArray *patientArray;
    NSMutableArray *bufferArray;
    Webservice *service;
    
    NSString *userName;
    NSString *password;
    NSString *searchText;
    IBOutlet UILabel *lblNoRecord;
    IBOutlet UISearchBar *searchBar;
    IBOutlet UILabel *lblLogout;
    UIImagePickerController *imagePicker;
    NSMutableDictionary *patientDict;
    NSInteger iRowSelect;
    NSInteger iTotalSelImages;
    UIPopoverController *libraryPopOver;
    
    int selectedrow;
}

@property (nonatomic, retain) UIImagePickerController *imagePicker;
@property (nonatomic,retain) NSMutableArray *patientArray;
@property (nonatomic, retain) NSMutableArray *bufferArray;
@property (nonatomic, retain) Webservice *service;
@property (nonatomic, retain) NSString *userName;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) IBOutlet UILabel *lblNoRecord;
@property (nonatomic, retain) NSString *searchText;
@property (nonatomic, retain) NSMutableDictionary *patientDict;
@property (nonatomic, retain) IBOutlet UILabel *lblLogout;
@property (nonatomic, retain) UIPopoverController *libraryPopOver;
- (void)loadPatients;
-(void)takeAPicture;
-(void)selectFromLibrary;
-(void)launchController;
-(IBAction)logoutBtnAction:(id)sender;
-(void)changeStuffAsPerLanguage;
- (UIImage *)portraitImageForImage:(UIImage *)image;
-(void)uploadImage:(UIImage*)image withCompression:(BOOL)compress;
@end
