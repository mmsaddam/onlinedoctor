//
//  SaveImageVC.h
//  Flowbook


#import <UIKit/UIKit.h>
#import "CustomImagePickerDelegate.h"
@interface SaveImageVC : UIViewController{
    UIButton *frameBtn;
    UIButton *flashBtn;
    UIButton *arrowBtn;
    UIButton *retakeBtn;
    UIButton *closeBtn;
    UIButton *tickBtn;
    UIButton *cameraBtn;
    UIImageView *cameraImage;
    
    UIImage *pictureImage;
    BOOL isFromShare;
    id <CustomImagePickerDelegate> delegate;
    UIButton *btnDone;
    UILabel *lblPreview;
}
@property (retain, nonatomic) IBOutlet UIButton *btnDone;
@property (nonatomic, retain) IBOutlet UILabel *lblPreview;

@property (nonatomic,retain) IBOutlet UIButton *frameBtn;
@property (nonatomic,retain) IBOutlet UIButton *flashBtn;
@property (nonatomic,retain) IBOutlet UIButton *arrowBtn;
@property (nonatomic,retain) IBOutlet UIButton *retakeBtn;
@property (nonatomic,retain) IBOutlet UIButton *closeBtn;
@property (nonatomic,retain) IBOutlet UIButton *tickBtn;
@property (nonatomic,retain) IBOutlet UIButton *cameraBtn;
@property (nonatomic,retain) IBOutlet UIImageView *cameraImage;
@property (nonatomic, retain) UIImage *pictureImage;
@property (nonatomic, assign) id <CustomImagePickerDelegate> delegate;
@property (nonatomic)BOOL isFromShare;

-(IBAction)saveBtnAction:(id)sender;
-(void)qualityBtnAction:(id)sender;
-(IBAction)retakeBtnAction:(id)sender;
- (UIImage *)portraitImageForImage:(UIImage *)image;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil image:(UIImage *)cameraimage;


@end
