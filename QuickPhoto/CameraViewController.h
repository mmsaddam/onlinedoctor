
#import "AVImageCaptureViewController.h"
#import "CustomImagePickerDelegate.h"
@interface CameraViewController : AVImageCaptureViewController{
    UIButton *closeBtn;
    UIButton *cameraBtn;
    IBOutlet UIButton *btnSwitchCamera;
    BOOL fromSave;
    UIImage *savedPhoto;
    id <CustomImagePickerDelegate> delegate;
}


@property (nonatomic,retain) IBOutlet UIButton *closeBtn;
@property (nonatomic,retain) IBOutlet UIButton *cameraBtn;
@property (nonatomic) BOOL fromSave;
@property (nonatomic, assign) id <CustomImagePickerDelegate> delegate;
- (IBAction)btnCancel:(id)sender;


- (IBAction)backBtnAction:(id)sender;

@end
