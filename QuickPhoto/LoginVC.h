




#import <UIKit/UIKit.h>
#import "BaseViewContoller.h"
#import "Webservice.h"

@interface LoginVC : BaseViewContoller<UITextFieldDelegate, WebserviceDelegate>{
    UIView *viewContainer;
    UITextField *loginTextfield;
    UITextField *passwordTextField;
    UISwitch *rememberSwitch;
    UIButton *btnLogin;
    UIButton *btnSignUp;
    UIButton *btnEnglish;
    UIButton *btnFrench;
    UILabel *lblRemember;
    UILabel *lblLanguage;
    Webservice *service;
}
@property (nonatomic, retain)  Webservice *service;
@property (nonatomic, retain) IBOutlet UITextField *loginTextfield;
@property (nonatomic, retain) IBOutlet UITextField *passwordTextField;
@property (nonatomic, retain) IBOutlet UISwitch *rememberSwitch;
@property (nonatomic, retain) IBOutlet UIButton *btnLogin;
@property (nonatomic, retain) IBOutlet UIButton *btnSignUp;
@property (nonatomic, retain) IBOutlet UIButton *btnEnglish;
@property (nonatomic, retain) IBOutlet UIButton *btnFrench;
@property (nonatomic, retain) IBOutlet UILabel *lblRemember;
@property (nonatomic, retain) IBOutlet UILabel *lblLanguage;
@property (retain, nonatomic) IBOutlet UIView *viewContainer;

-(IBAction)loginBtnAction:(id)sender;
-(IBAction)signUpBtnAction:(id)sender;
-(IBAction)remembermeAction:(id)sender;
-(void)loginAction;
-(BOOL)isValidData;

-(IBAction)franchBtnAction:(id)sender;
-(IBAction)englishBtnAction:(id)sender;
-(void)changeStuffAsPerLanguage;


-(void)callLoginWebservice;
@end
