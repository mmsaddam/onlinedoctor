//
//  PatientCellView.m
//  QuickPhoto
//
//  Created by Sujal on 28/09/12.
//  Copyright (c) 2012 TriState. All rights reserved.
//

#import "PatientCellView.h"

@interface PatientCellView ()

@end

@implementation PatientCellView
@synthesize nameLbl;
@synthesize birthdateLbl;
@synthesize phoneLbl;
@synthesize bgImgView;
@synthesize lblDOBHeader;

-(void)dealloc{
    [nameLbl release];
    [birthdateLbl release];
    [phoneLbl release];
    self.lblDOBHeader = nil;
    self.bgImgView = nil;
    [super dealloc];
}

@end
