
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class CameraViewController;

@protocol CustomImagePickerDelegate <NSObject>

- (void)customImagePickerController:(CameraViewController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
- (void)cancelCustomImagePickerController:(CameraViewController *)picker;
@end
