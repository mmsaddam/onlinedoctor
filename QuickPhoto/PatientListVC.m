
#import "PatientListVC.h"
#import "UITableViewCell+NIB.h"
#import "PatientCellView.h"
#import "NSString+MD5Addition.h"
#import "MKInfoPanel.h"
#import "ELCAlbumPickerController.h"
#import "ELCImagePickerController.h"
#import "ALAsset+Export.h"
#import "NSURL+Extension.h"
#import "CameraViewController.h"
#import "UIDevice+Resolutions.h"

//7874512532

@interface PatientListVC ()

@end

@implementation PatientListVC
@synthesize patientArray;
@synthesize service;
@synthesize userName;
@synthesize password;
@synthesize bufferArray;
@synthesize lblNoRecord;
@synthesize searchText;
@synthesize imagePicker;
@synthesize patientDict;
@synthesize lblLogout;
@synthesize libraryPopOver;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

 //   self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.title = @"Patients";
    self.service = [[[Webservice alloc] init] autorelease];
    self.service._delegate = self;
    self.searchText = @"";
    [self loadPatients];
    self.patientArray = [NSMutableArray array];
    self.bufferArray = [NSMutableArray array];
    iRowSelect = NSNotFound;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    self.imagePicker = picker;
    [picker release];
    [self changeStuffAsPerLanguage];
    
}

#pragma mark - TableView Methods
#pragma mark -

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    return [self.patientArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableV cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PatientCellView *cell;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        cell= [PatientCellView dequeOrCreateInTable:tableV];
    else
        cell = [PatientCellView dequeOrCreateInTable:tableV withNibname:@"PatientCellView_iPad"];
    if([patientArray count] <= indexPath.row)
        return cell;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSMutableDictionary *dataDict = [patientArray objectAtIndex:indexPath.row];
    cell.nameLbl.text = [NSString stringWithFormat:@"%@ %@",[dataDict objectForKey:@"last_name"],[dataDict objectForKey:@"first_name"]];
    NSString *phone = [dataDict objectForKey:@"phone"];
    if(!phone)
        phone = @"";
    if((NSNull*)phone == [NSNull null])
        phone = @"";
    
    NSString *dob = [dataDict objectForKey:@"dob"];
    if(!dob)
        dob = @"";
    
    if((NSNull*)dob == [NSNull null])
        dob = @"";
    if(iRowSelect == indexPath.row){
        cell.bgImgView.highlighted = YES;
        cell.lblDOBHeader.textColor = [UIColor whiteColor];
        cell.nameLbl.textColor = [UIColor whiteColor];
        cell.birthdateLbl.textColor = [UIColor whiteColor];
    }
    else{
        cell.bgImgView.highlighted = NO;
        cell.lblDOBHeader.textColor = [UIColor blackColor];
        cell.nameLbl.textColor = [UIColor blackColor];
        cell.birthdateLbl.textColor = [UIColor blackColor];
    }
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    if([str isEqualToString:@"en"]){
        cell.lblDOBHeader.text = @"DOB";
    }
    else{
        cell.lblDOBHeader.text = @"DDN";
    }
    cell.phoneLbl.text = phone;
    cell.birthdateLbl.text = dob;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

- (void)tableView:(UITableView *)tableV didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray *visible = [tableView indexPathsForVisibleRows];
    
    NSIndexPath *indexpath1 = (NSIndexPath*)[visible objectAtIndex:0];
    
    if(indexpath1.row>0){
        selectedrow = indexPath.row - indexpath1.row;
    }
    else{
         selectedrow = indexPath.row;
    }
    
   
    [searchBar resignFirstResponder];
    if([patientArray count] <= indexPath.row)
        return;
    if([self egoRefreshTableHeaderDataSourceIsLoading:nil])
        return;
    
    for(PatientCellView *cell in [tableV visibleCells]){
        cell.bgImgView.highlighted = NO;
        cell.lblDOBHeader.textColor = [UIColor blackColor];
        cell.nameLbl.textColor = [UIColor blackColor];
        cell.birthdateLbl.textColor = [UIColor blackColor];
    }
    iRowSelect = indexPath.row;
    PatientCellView *cell = (PatientCellView*)[tableV cellForRowAtIndexPath:indexPath];
    cell.bgImgView.highlighted = YES;
    cell.lblDOBHeader.textColor = [UIColor whiteColor];
    cell.nameLbl.textColor = [UIColor whiteColor];
    cell.birthdateLbl.textColor = [UIColor whiteColor];
    
    NSMutableDictionary *dict = [patientArray objectAtIndex:indexPath.row];
    self.patientDict = dict;
    
    UIActionSheet *sheet;
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    if([str isEqualToString:@"en"]){
        cell.lblDOBHeader.text = @"DOB";
        sheet = [[UIActionSheet alloc] initWithTitle:@"Select any option to upload image:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take a picture",@"Select from library", nil];
    }
    else{
        cell.lblDOBHeader.text = @"DDN";
        sheet = [[UIActionSheet alloc] initWithTitle:@"Sélectionnez une option pour télécharger l'image:" delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Prendre une photo",@"Choisir dans une bibliothèque", nil];
    }
    sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [sheet showInView:self.view];
    [sheet release];

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(![self isNetworkReachable]){
        NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
        if([str isEqualToString:@"en"]){
            [UIAlertView infoAlertWithMessage:@"Please check your internet Connection." andTitle:AlertName];
            
        }
        else if([str isEqualToString:@"fr"]){
            [UIAlertView infoAlertWithMessage:@"S'il vous plaît vérifier votre connexion Internet." andTitle:AlertName];
        }
        return;
    }
         
    if(buttonIndex == 0){
        [self takeAPicture];
    }
    else if(buttonIndex == 1){
 //       [self selectFromLibrary]
        [self launchController];
       
    }
}

-(void)takeAPicture{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        NSLog(@"No camera!");
        UIAlertView *cameraAlert=[[UIAlertView alloc] initWithTitle:AlertName message:@"No Camera" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
        [cameraAlert show];
        [cameraAlert release];
    }
    else {
//        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
 //       [self presentModalViewController: imagePicker animated:YES];
        
        NSString *nibName = @"CameraViewController";
        UIDevice *device = [UIDevice currentDevice];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            nibName = @"CameraViewController_iPad";
        if(device.resolution == UIDeviceResolution_iPhoneRetina4)
            nibName = @"CameraViewController_iPhone5";
        
        CameraViewController *cameraViewController = [[CameraViewController alloc] initWithNibName:nibName bundle:nil];
        cameraViewController.delegate = self;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:cameraViewController];
        navigationController.navigationBar.hidden= YES;
        [self presentModalViewController:navigationController animated:YES];
        [cameraViewController release];
        [navigationController release];
    }
}
-(void)selectFromLibrary{
    if([UIImagePickerController isSourceTypeAvailable:
        UIImagePickerControllerSourceTypePhotoLibrary]){
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentModalViewController: imagePicker animated:YES];
    }
}
-(void)changeStuffAsPerLanguage{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    if([str isEqualToString:@"en"]){
        lblLogout.text = @"Logout";
        lblNoRecord.text = @"No records found.. \npull screen to refresh content.";
        
    }
    else if([str isEqualToString:@"fr"]){
        lblLogout.text = @"Déconnexion";
        lblNoRecord.text = @"Aucun enregistrement trouvé ..\n tirez l'écran pour actualiser le contenu.";
    }
}
#pragma mark -
#pragma mark Image Picker delegate
#pragma mark -

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    iTotalSelImages = 1;
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    image = [self portraitImageForImage:image];
    [self performSelector:@selector(uploadImage:) withObject:image afterDelay:0.3];
    [self dismissModalViewControllerAnimated:YES];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissModalViewControllerAnimated:YES];
}
- (void)customImagePickerController:(CameraViewController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    iTotalSelImages = 1;
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [self performSelector:@selector(uploadImage:) withObject:image afterDelay:0.1];
    [self dismissModalViewControllerAnimated:YES];
}
-(void)uploadImage:(UIImage*)image{
    image = [self portraitImageForImage:image];
    [self uploadImage:image withCompression:NO];
}
-(void)uploadImage:(UIImage*)image withCompression:(BOOL)compress{
    NSData *data = nil;
    if(compress)
        data = UIImageJPEGRepresentation(image, 0.6);
    else
        data = UIImageJPEGRepresentation(image, 1.0);
    
    if([self isNetworkReachable]){
        if(self.service){
            [self.service cancelWebservice];
        }
        iTotalSelImages--;
        NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
        if([str isEqualToString:@"en"]){
            [self showHudWithText:@"Uploading..."];
        }
        else{
            [self showHudWithText:@"Téléchargement..."];
        }
        
        NSString *pass = [self.password stringFromMD5];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:userName forKey:@"u"];
        [dict setObject:pass forKey:@"p"];
        [dict setObject:[NSString stringWithFormat:@"%@",[self.patientDict objectForKey:@"id"]] forKey:@"patient_id"];
        service.iTag = 1;
        [service callpostImage:data andParams:dict extension:@"patient/save_photo.ashx"];
    }
    else{
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
        if([str isEqualToString:@"en"]){
            [UIAlertView infoAlertWithMessage:@"Please check your internet Connection. Photo has been saved in Photo Gallery." andTitle:AlertName];
            
        }
        else if([str isEqualToString:@"fr"]){
            [UIAlertView infoAlertWithMessage:@"S'il vous plaît vérifier votre connexion Internet. Photo a été enregistré dans la Galerie Photo." andTitle:AlertName];
        }
    }
}

#pragma mark -
#pragma mark Webservice Methods
#pragma mark -



-(void)loadPatientsForText:(NSString*)strKey withStartIndex:(NSInteger)startIndex andPageCount:(NSInteger)pageCount{
    if([self isNetworkReachable]){
        if(self.service){
            [self.service cancelWebservice];
        }
        NSString *pass = [self.password stringFromMD5];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:self.userName forKey:@"u"];
        [dict setObject:pass forKey:@"p"];
        [dict setObject:strKey forKey:@"filter"];
        [dict setObject:[NSString stringWithFormat:@"%d",startIndex] forKey:@"offset"];
        [dict setObject:[NSString stringWithFormat:@"%d",pageCount] forKey:@"limit"];
        service.iTag = 0;
        [service callJSONGETWebserviceMethod:@"patient/list.ashx" withParams:dict];
    }
    else{
        NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
        if([str isEqualToString:@"en"]){
            [UIAlertView infoAlertWithMessage:@"Please check your internet Connection." andTitle:AlertName];
            
        }
        else if([str isEqualToString:@"fr"]){
            [UIAlertView infoAlertWithMessage:@"S'il vous plaît vérifier votre connexion Internet." andTitle:AlertName];
        }
    }
}

-(void)completeDownload:(id)dict withWebservice:(Webservice*)serv{
    if(service.iTag){
        if(iTotalSelImages==0){
            [self hidHud];
            NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
            if([str isEqualToString:@"en"]){
                [UIAlertView infoAlertWithMessage:@"Image has been uploaded successfully." andTitle:@"Success"];
                
            }
            else if([str isEqualToString:@"fr"]){
                [UIAlertView infoAlertWithMessage:@"l'image a été téléchargé!" andTitle:@"Succès"];
            }
        }
    }
    else{
        NSArray *result = [dict objectForKey:@"patients"];
        [spinner stopAnimating];
        if([result count]){
            [bufferArray addObjectsFromArray:result];
            self.patientArray = bufferArray;
            [tableView reloadData];
            [tableView setTableHeaderView:nil];
            [tableView setTableFooterView:nil];
            lblNoRecord.hidden = YES;
        }
        else {
            if([patientArray count]==0){
                self.patientArray = nil;
                [self.bufferArray removeAllObjects];
                [tableView reloadData];
                [tableView setTableHeaderView:lblNoRecord];
                [tableView setTableFooterView:nil];
                lblNoRecord.hidden = NO;
            }
            else{
                [MKInfoPanel showPanelInView:self.view
                                        type:MKInfoPanelTypeError
                                       title:@"Unknown Error!!"
                                    subtitle:nil
                                   hideAfter:2.5];
            }
            return;
        }
        [self doneLoadingTableViewData];
    }
}
-(void)failWithWebService:(Webservice*)serv error:(NSError*)error{
    if(service.iTag == 1){
        if(iTotalSelImages==0){
            [self hidHud];
        }
    }
    else{
        [self doneLoadingTableViewData];
        [spinner stopAnimating];
    }
    
}

#pragma mark -
#pragma mark SearchBar Delegate
#pragma mark -

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchB{
    [searchBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchB{
    [searchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchB{
    self.searchText = @"";
    [searchBar resignFirstResponder];
}
- (void)searchBar:(UISearchBar *)searchBa textDidChange:(NSString *)searchText{
    self.searchText = searchBar.text;
    [self loadPatients];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchr{
    [searchBar resignFirstResponder];
}
#pragma mark -
#pragma mark Action Zone
#pragma mark -

-(IBAction)logoutBtnAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ECL


-(void)launchController {
    
  //  int test = selectedrow;
    
    ELCAlbumPickerController *albumController = [[ELCAlbumPickerController alloc] initWithNibName:@"ELCAlbumPickerController" bundle:[NSBundle mainBundle]];    
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initWithRootViewController:albumController];
    [albumController setParent:elcPicker];
    [elcPicker setDelegate:self];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        albumController.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        [self.navigationController presentModalViewController:elcPicker animated:YES];
        
    }
    else{
        self.libraryPopOver = [[[UIPopoverController alloc] initWithContentViewController:elcPicker] autorelease];
        libraryPopOver.popoverContentSize = CGSizeMake(300.0, 300.0);
        [libraryPopOver presentPopoverFromRect:CGRectMake(83.0, selectedrow*60+92, 44.0, 44.0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    
        
    [elcPicker release];
    [albumController release];
}

#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info {
	iTotalSelImages = [info count];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        [self dismissModalViewControllerAnimated:YES];
    else
        [self.libraryPopOver dismissPopoverAnimated:YES];
	for(NSDictionary *dict in info) {
        NSString *mediaType = [dict objectForKey:UIImagePickerControllerMediaType];
        if([mediaType isEqualToString:ALAssetTypePhoto]){
            
            while (service.iTag == 1 && service.busy && [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]);
            
            NSString *path = [@"TestImage.jpg" pathInDocumentDirectory];
            ALAsset *asset = [dict objectForKey:@"Asset"];
            [asset exportDataToURL:[NSURL fileURLWithPath:path] error:nil];
            
            BOOL isFlagAdded = [[NSURL fileURLWithPath:path] addSkipBackupAttribute];
            NSLog(@"%d",isFlagAdded);
            
            NSData *data = [NSData dataWithContentsOfFile:path options:0 error:nil];
            UIImage *image = [UIImage imageWithData:data];
            image = [self portraitImageForImage:image];
            [self uploadImage:image withCompression:YES];
        }
	}
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker {
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        [self dismissModalViewControllerAnimated:YES];
    else
        [self.libraryPopOver dismissPopoverAnimated:YES];
}
- (UIImage *)portraitImageForImage:(UIImage *)image
{
    int kMaxResolution = 3000; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
//    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
//    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    /*switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }*/
    
    
    //
//    NSString *stringPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"New Folder"];
//    NSError *error = nil;
//    if (![[NSFileManager defaultManager] fileExistsAtPath:stringPath])
//        [[NSFileManager defaultManager] createDirectoryAtPath:stringPath withIntermediateDirectories:NO attributes:nil error:&error];
//    
//    NSString *fileName = [stringPath stringByAppendingFormat:[NSString stringWithFormat:@"/%f.png",[[NSDate date] timeIntervalSince1970]]];
//    NSData *data = UIImageJPEGRepresentation(image, 1.0);
//    [data writeToFile:fileName atomically:YES];
//    
    
    //
    
    
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
    
    //    return image;
    
    //    return [UIImage imageWithCGImage:[image CGImage] scale:1.0 orientation:UIImageOrientationUp];
}
#pragma mark -
#pragma mark Paging and Refresh
#pragma mark -
- (void)loadPatients{
    iPageNumber=0;
    [self.bufferArray removeAllObjects];
    [spinner startAnimating];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        [self loadPatientsForText:searchText withStartIndex:iPageNumber andPageCount:PAGECOUNT];
    else
        [self loadPatientsForText:searchText withStartIndex:iPageNumber andPageCount:PAGECOUNT_iPad];
    [tableView setContentOffset:CGPointMake(0.0, 0.0)];
}
-(void)refreshTableView{
    [self.bufferArray removeAllObjects];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        [self loadPatientsForText:searchText withStartIndex:0 andPageCount:(iPageNumber*PAGECOUNT)];
    else
        [self loadPatientsForText:searchText withStartIndex:0 andPageCount:(iPageNumber*PAGECOUNT_iPad)];
    
}
- (NSInteger)totalCount{
    return [self.patientArray count];
}
- (void)loadMore{
    self.bufferArray = patientArray;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        [self loadPatientsForText:searchText withStartIndex:((iPageNumber)*PAGECOUNT) andPageCount:(PAGECOUNT)];
    else
        [self loadPatientsForText:searchText withStartIndex:((iPageNumber)*PAGECOUNT_iPad) andPageCount:(PAGECOUNT_iPad)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc{
    if(service){
        [self.service cancelWebservice];
        self.service._delegate = nil;
        self.service = nil;
    }
    self.libraryPopOver = nil;
    [searchBar release];
    self.lblNoRecord = nil;
    self.bufferArray = nil;
    self.patientArray = nil;
    self.userName = nil;
    self.password = nil;
    self.patientDict = nil;
    self.lblLogout = nil;
    [super dealloc];
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}
@end
