//
//  PatientCellView.h
//  QuickPhoto
//
//  Created by Sujal on 28/09/12.
//  Copyright (c) 2012 TriState. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatientCellView : UITableViewCell{
    UILabel *nameLbl;
    UILabel *birthdateLbl;
    UILabel *phoneLbl;
    UIImageView *bgImgView;
    IBOutlet UILabel *lblDOBHeader;
}

@property (nonatomic, retain) IBOutlet UILabel *nameLbl;
@property (nonatomic, retain) IBOutlet UILabel *birthdateLbl;
@property (nonatomic, retain) IBOutlet UILabel *phoneLbl;
@property (nonatomic, retain) IBOutlet UIImageView *bgImgView;
@property (nonatomic, retain) IBOutlet UILabel *lblDOBHeader;

@end
