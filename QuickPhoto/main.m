//
//  main.m
//  QuickPhoto
//
//  Created by Sujal on 28/09/12.
//  Copyright (c) 2012 TriState. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
