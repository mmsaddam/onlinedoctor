//
//  AppDelegate.h
//  QuickPhoto
//
//  Created by Sujal on 28/09/12.
//  Copyright (c) 2012 TriState. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoginVC;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) LoginVC *viewController;

@property (nonatomic,retain) UINavigationController *navigationCotroller;

@end
