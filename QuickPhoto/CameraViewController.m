

#import "CameraViewController.h"
#import "AppDelegate.h"
#import "NSString+Extensions.h"
#import "SaveImageVC.h"
#import "DDExpandableButton.h"
#import "UIDevice+Resolutions.h"
#define radians( degrees ) ( degrees * M_PI / 180 )

@implementation CameraViewController
@synthesize closeBtn;
@synthesize cameraBtn;
@synthesize fromSave;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    // Do any additional setup after loading the view from its nib.
    if (NSClassFromString(@"AVCaptureDevice") != nil)
	{
		AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
		BOOL isIos7 =  [[[UIDevice currentDevice] systemVersion] floatValue] >= 7?YES:NO;
        
        if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad){
            if (isIos7) {
                btnSwitchCamera.frame = CGRectMake(260, 0, 30, 30);
                [btnSwitchCamera setImage:[UIImage imageNamed:@"camera icon.png"] forState:UIControlStateNormal];
                cameraBtn.frame = CGRectMake(cameraBtn.frame.origin.x, cameraBtn.frame.origin.y, 68, 68);
                [cameraBtn setImage:[UIImage imageNamed:@"camera_bt.png"] forState:UIControlStateNormal];
            }
            else
                btnSwitchCamera.frame = CGRectMake(240, 10, 69, 42);
        }
        
		if ([device hasFlash]){
            DDExpandableButton *torchModeButton = [[[DDExpandableButton alloc] initWithPoint:CGPointMake(10.0,isIos7?0.0:10.0)
                                                                                   leftTitle:[UIImage imageNamed:isIos7?@"Flash1.png":@"Flash.png"]
                                                                                     buttons:[NSArray arrayWithObjects:@"Auto", @"On", @"Off", nil]] autorelease];
            [[self view] addSubview:torchModeButton];
            [torchModeButton addTarget:self action:@selector(toggleFlashlight:) forControlEvents:UIControlEventValueChanged];
            [torchModeButton setVerticalPadding:6];
            [torchModeButton updateDisplay];
            
            [torchModeButton setSelectedItem:[[NSUserDefaults standardUserDefaults] integerForKey:@"falshmode"]];
            [self toggleFlashlight:torchModeButton];

        }
    }
    if([[NSUserDefaults standardUserDefaults] integerForKey:@"cameramode"]){
        [self switchCameras:nil];
    }
    [self changeStuffAsPerLanguage];
}
-(void)changeStuffAsPerLanguage{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    if([str isEqualToString:@"en"]){
        [closeBtn setTitle:@"Cancel" forState:UIControlStateNormal];
        
    }
    else if([str isEqualToString:@"fr"]){
       [closeBtn setTitle:@"Annuler" forState:UIControlStateNormal];
    }
}
- (IBAction)switchCameras:(id)sender{
    [super switchCameras:sender];
    [[NSUserDefaults standardUserDefaults] setInteger:isUsingFrontFacingCamera forKey:@"cameramode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    BOOL saveImage = [[NSUserDefaults standardUserDefaults] boolForKey:@"SaveImage"];
    
    if(saveImage){
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"SaveImage"];
        [self sharecamreraImage];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (NSClassFromString(@"AVCaptureDevice") != nil)
	{
		AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
		
		if ([device hasTorch])
		{
			[device lockForConfiguration:nil];
			[device setTorchMode:AVCaptureTorchModeOff];
			[device unlockForConfiguration];
		}
	}
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)capturedImage:(UIImage *)image Tag:(int)tag{
    NSString *nibName = @"SaveImageVC";
    UIDevice *device = [UIDevice currentDevice];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        nibName = @"SaveImageVC_iPad";
    if(device.resolution == UIDeviceResolution_iPhoneRetina4)
        nibName = @"SaveImageVC_iPhone5";
    
    SaveImageVC *saveImage = [[SaveImageVC alloc] initWithNibName:nibName bundle:nil image:image];
    saveImage.delegate = delegate;
    [self.navigationController pushViewController:saveImage animated:YES];
    [saveImage release];
}

-(void)sharecamreraImage{
    
    NSMutableArray *arrControllers=[[NSMutableArray alloc]initWithArray:self.navigationController.viewControllers];
//    UIViewController *tabbar = [arrControllers objectAtIndex:1];
    
    [self.navigationController popViewControllerAnimated:NO];
    [arrControllers release];
}


- (IBAction)btnCancel:(id)sender {
}

- (IBAction)backBtnAction:(id)sender{
    
    [self dismissModalViewControllerAnimated:YES];
}
- (void)toggleFlashlight:(DDExpandableButton *)sender
{
	if (NSClassFromString(@"AVCaptureDevice") != nil)
	{
		AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
		
		if ([device hasFlash])
		{
			[device lockForConfiguration:nil];
			[device setFlashMode:(2 - [sender selectedItem])];
			[device unlockForConfiguration];
		}
	}
    [[NSUserDefaults standardUserDefaults] setInteger:[sender selectedItem] forKey:@"falshmode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)dealloc{
    self.delegate = nil;
    [savedPhoto release];
    savedPhoto = nil;
    self.closeBtn = nil;
    self.cameraBtn = nil;
    [super dealloc];
}
@end
