

#import "LoginVC.h"
#import "PatientListVC.h"
#import "NSString+MD5Addition.h"
#import "NSData+AES256.h"
#import "UIViewController+KeyboardFrame.h"


@interface LoginVC ()

@end

@implementation LoginVC
@synthesize viewContainer;
@synthesize passwordTextField;
@synthesize loginTextfield;
@synthesize rememberSwitch;
@synthesize service;
@synthesize btnLogin;
@synthesize btnSignUp;
@synthesize btnEnglish;
@synthesize btnFrench;
@synthesize lblRemember;
@synthesize lblLanguage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBarHidden = YES;
    self.title = @"Login";
    
    self.service = [[[Webservice alloc] init] autorelease];
    self.service._delegate = self;
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    if(!str){
        [self englishBtnAction:nil];
    }
    [self changeStuffAsPerLanguage];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    BOOL rememberFlag = [[NSUserDefaults standardUserDefaults] boolForKey:@"RememberMe"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if(rememberFlag){
        [rememberSwitch setOn:YES];
        NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
        
        
        NSData *encryptedData = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
        NSData *data = [encryptedData AES256DecryptWithKey:@"quickphotopass"];
        NSString *strData = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]  autorelease];
        
        loginTextfield.text = username;
        passwordTextField.text = strData;
    }
    else{
        loginTextfield.text = @"";
        passwordTextField.text = @"";
        [rememberSwitch setOn:NO];
    }
}

-(void)changeStuffAsPerLanguage{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    if([str isEqualToString:@"en"]){
        loginTextfield.placeholder = @"Enter Username";
        passwordTextField.placeholder = @"Enter Password";
        lblRemember.text = @"Remember me";
        lblLanguage.text = @"Language";
        [btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        [btnSignUp setTitle:@"Signup" forState:UIControlStateNormal];
        [btnEnglish setSelected:YES];
        [btnFrench setSelected:NO];
        
    }
    else if([str isEqualToString:@"fr"]){
        loginTextfield.placeholder = @"Nom d'utilisateur";
        passwordTextField.placeholder = @"Mot de passe";
        lblRemember.text = @"Se souvenir de moi";
        lblLanguage.text = @"Language";
        [btnLogin setTitle:@"Connexion" forState:UIControlStateNormal];
        [btnSignUp setTitle:@"Inscrivez-vous" forState:UIControlStateNormal];
        [btnEnglish setSelected:NO];
        [btnFrench setSelected:YES];
    }
}

-(BOOL)isValidData{
    
    if([loginTextfield.text length] == 0){
        [MKInfoPanel showPanelInView:self.view
                                type:MKInfoPanelTypeError
                               title:@"Please enter email id."
                            subtitle:nil
                           hideAfter:2.5];
        return NO;
    }
    if([passwordTextField.text length] == 0){
        [MKInfoPanel showPanelInView:self.view
                                type:MKInfoPanelTypeError
                               title:@"Please enter Password."
                            subtitle:nil
                           hideAfter:2.5];
        return NO;
    }
       return YES;
}


-(IBAction)loginBtnAction:(id)sender{
    if([self isValidData]){
        [loginTextfield resignFirstResponder];
        [passwordTextField resignFirstResponder];
        [self callLoginWebservice];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if(textField==loginTextfield)
        [passwordTextField becomeFirstResponder];
    else if(textField==passwordTextField)
        [passwordTextField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.viewContainer setViewframe:textField forSuperView:self.view];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.viewContainer resetViewframe];
    
}

#pragma mark -
#pragma mark Webservice Methods
#pragma mark -

-(void)callLoginWebservice{
    if([self isNetworkReachable]){
        [self showHud];
        NSString *userName = loginTextfield.text;
        NSString *password = [passwordTextField.text stringFromMD5];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:userName forKey:@"u"];
        [dict setObject:password forKey:@"p"];
        
        [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"username"];
        
        NSData *data = [passwordTextField.text dataUsingEncoding:NSUTF8StringEncoding];
        NSData *encryptedData = [data AES256EncryptWithKey:@"quickphotopass"];
        [[NSUserDefaults standardUserDefaults] setObject:encryptedData forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [service callJSONGETWebserviceMethod:@"user/login.ashx" withParams:dict];
    }
    else{
        [UIAlertView infoAlertWithMessage:@"Please check your internet Connection" andTitle:AlertName];
    }
}
-(void)completeDownload:(id)dict withWebservice:(Webservice*)service{
    NSInteger isAuthenticate = [[dict objectForKey:@"authentication"] intValue];
    if(!isAuthenticate){
        [self loginAction];
    }
    else{
        [UIAlertView infoAlertWithMessage:@"Please enter valid username or password." andTitle:AlertName];
    }
    [self hidHud];
}
-(void)failWithWebService:(Webservice*)service error:(NSError*)error{
    [self hidHud];
    [UIAlertView infoAlertWithMessage:[error localizedDescription] andTitle:AlertName];
}
#pragma mark -
#pragma mark Action Methods
#pragma mark -

-(IBAction)signUpBtnAction:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://medesync.com"]];
}
-(void)loginAction{
    NSString *nibName = @"PatientListVC";
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        nibName = @"PatientListVC_iPad";
    PatientListVC *patientvc = [[PatientListVC alloc] initWithNibName:nibName bundle:nil];
    patientvc.userName = loginTextfield.text;
    patientvc.password = passwordTextField.text;
    [self.navigationController pushViewController:patientvc animated:YES];
    [patientvc release];
}


-(IBAction)remembermeAction:(id)sender{
    
    if(rememberSwitch.on){
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"RememberMe"];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"RememberMe"];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(IBAction)englishBtnAction:(id)sender{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    if(![str isEqualToString:@"en"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"lang"];
        [self changeStuffAsPerLanguage];
    }
    [btnEnglish setSelected:YES];
    [btnFrench setSelected:NO];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(IBAction)franchBtnAction:(id)sender{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    
    if(![str isEqualToString:@"fr"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"fr" forKey:@"lang"];
        [self changeStuffAsPerLanguage];
    }
    [btnEnglish setSelected:NO];
    [btnFrench setSelected:YES];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc{
    if(service){
        [self.service cancelWebservice];
        self.service._delegate = nil;
        self.service = nil;
    }
    [loginTextfield release];
    [passwordTextField release];
    [btnLogin release];
    [btnSignUp release];
    [btnEnglish  release];
    [btnFrench release];
    self.lblRemember = nil;
    self.lblLanguage = nil;
    [super dealloc];
}

@end
