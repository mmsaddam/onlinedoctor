

#import <UIKit/UIKit.h>

    CGFloat animatedDistance;

@interface UIView (KeyboardFrame)
-(UIView *)setViewframe:(UITextField *)textField forSuperView:(UIView *)superView;
-(UIView *)setViewframe1:(UITextView *)textview forSuperView:(UIView *)superView;
-(UIView *)resetViewframe;
-(CGFloat)getAnimatedDistance:(CGFloat)heightFraction;
@end
