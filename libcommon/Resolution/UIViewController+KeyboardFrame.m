


//    [self.view1 setViewframe:textField forSuperView:self.view]; // to set

//    [self.view1 resetViewframe:textField]; // to reset


#import "UIViewController+KeyboardFrame.h"
#import "UIDevice+Resolutions.h"

@implementation UIView (KeyboardFrame)

-(UIView *)setViewframe:(UITextField *)textField forSuperView:(UIView *)superView{
    
    CGRect textFieldRect =[superView convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [superView convertRect:superView.bounds fromView:superView];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - 0.2 * viewRect.size.height;
    CGFloat denominator = 0.6 * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
        heightFraction = 0.0;
    else if (heightFraction > 1.0)
        heightFraction = 1.0;
    
    animatedDistance = [self getAnimatedDistance:heightFraction];
    CGRect viewFrame1 = self.frame;
    viewFrame1.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    [self setFrame:viewFrame1];
    [UIView commitAnimations];
    return self;
    
}

-(UIView *)resetViewframe{
    CGRect viewFrame1 = self.frame;
    viewFrame1.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    
    [self setFrame:viewFrame1];
    
    [UIView commitAnimations];
    return self;
}


-(UIView *)setViewframe1:(UITextView *)textview forSuperView:(UIView *)superView{
    
    CGRect textFieldRect =[superView convertRect:textview.bounds fromView:textview];
    CGRect viewRect = [superView convertRect:superView.bounds fromView:superView];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - 0.2 * viewRect.size.height;
    CGFloat denominator = 0.6 * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
        heightFraction = 0.0;
    else if (heightFraction > 1.0)
        heightFraction = 1.0;
    
    animatedDistance = [self getAnimatedDistance:heightFraction];
    CGRect viewFrame1 = self.frame;
    viewFrame1.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    [self setFrame:viewFrame1];
    [UIView commitAnimations];
    return self;
    
}


-(CGFloat)getAnimatedDistance:(CGFloat)heightFraction{
 
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
        if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            animatedDistance = floor(264 * heightFraction);
        }
        else
        {
            animatedDistance = floor(352 * heightFraction);
        }
    }
    else {
        UIInterfaceOrientation orientation =[[UIApplication sharedApplication] statusBarOrientation];
        if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            animatedDistance = floor(216 * heightFraction);
        }
        else
        {
            animatedDistance = floor(162 * heightFraction);
        }
    }
    return animatedDistance;
}



//-(CGRect)getframe{
//    UIWindow *window = [[UIApplication sharedApplication] delegate].window;
//    CGRect viewFrame1 = window.frame;
//    return viewFrame1;
//}



@end
