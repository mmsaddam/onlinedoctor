//
//  ParentViewController.m
//  Restaurant
//
//  Created by Darshit on 16/10/13.
//  Copyright (c) 2013 TriState Technology. All rights reserved.
//

#import "ParentViewController.h"
#import "UIDevice+Resolutions.h"
@interface ParentViewController ()

@end

@implementation ParentViewController
@synthesize isStatusBarSet;

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    if (!self.isStatusBarSet) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
            self.view.clipsToBounds = YES;
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenHeight = screenRect.size.height;
            self.view.frame =  CGRectMake(0, 20, self.view.frame.size.width,screenHeight-20);
            self.view.bounds = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        }
    }
    [self.view setNeedsDisplay];
}



-(BOOL)isiOS7{
    return [[[UIDevice currentDevice] systemVersion] floatValue] >= 7?YES:NO;
}

-(BOOL)isForIphone4{
    UIDevice *currentDevice = [UIDevice currentDevice];
    return [currentDevice resolution] == UIDeviceResolution_iPhoneRetina4?YES:NO;
}

-(BOOL)isForIpad{
    UIDevice *currentDevice = [UIDevice currentDevice];
    return [currentDevice resolution] == UIDeviceResolution_iPadStandard?YES:NO;
}



@end
