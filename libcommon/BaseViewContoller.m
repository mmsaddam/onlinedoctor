

#import "BaseViewContoller.h"
#import "NetworkReachability.h"

@implementation BaseViewContoller
@synthesize spinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)isNetworkReachable{
    
    return [self isNetworkReachable:@"http://www.google.com"];
}
- (BOOL)isNetworkReachable:(NSString*)NetPath 
{
	[[NetworkReachability sharedReachability] setHostName:[NSString stringWithFormat:@"%@",NetPath]];
	NetworkReachbilityStatus remoteHostStatus = [[NetworkReachability sharedReachability] internetConnectionStatus];
	
	if (remoteHostStatus == NetworkNotReachable)
		return NO;
	else if((remoteHostStatus == ReachableViaCarrierDataNetwork) || (remoteHostStatus == ReachableViaWiFiNetwork))
		return YES;
	return NO;
}
- (void)showHud {
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"lang"];
    if([str isEqualToString:@"en"]){
        [self showHudWithText:@"Loading.."];
    }
    else{
        [self showHudWithText:@"Chargement.."];
    }
    
}
- (void)showHudWithText:(NSString*)txt {
    if (!HUD) {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        HUD.labelText = txt;
        [HUD show:YES];
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
}

- (void)hidHud {
	if (HUD) {
		[HUD show:NO];
		[HUD removeFromSuperview];
		[HUD release];
		HUD = nil;
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
	}
}
- (void)showSpinnerWithUserActionEnable:(BOOL)isEnable{
    [spinner startAnimating];
    self.view.userInteractionEnabled = isEnable;
}
- (void)hideSpinner{
    [spinner stopAnimating];
    self.view.userInteractionEnabled = YES;
}
#pragma mark - View lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.navigationController.navigationBarHidden = YES;
    viewFrame = [self.view bounds];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameWillChange:) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
    intialStatuHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    intialStatuHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
}
- (void)statusBarFrameWillChange:(NSNotification*)notification {
    NSValue* rectValue = [[notification userInfo] valueForKey:UIApplicationStatusBarFrameUserInfoKey];
    CGRect newFrame;
    [rectValue getValue:&newFrame];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.40];
    if(newFrame.size.height == 40){
        if(intialStatuHeight == 40){
            [self.view setFrame:CGRectMake(0.0, 0.0, viewFrame.size.width, viewFrame.size.height)];
        }
        else{
            [self.view setFrame:CGRectMake(0.0, 20.0, viewFrame.size.width, viewFrame.size.height)];
        }
        
    }
    else{
        if(intialStatuHeight == 40){
            [self.view setFrame:CGRectMake(0.0, -20, viewFrame.size.width, viewFrame.size.height)];
        }
        else{
            [self.view setFrame:CGRectMake(0.0, 0.0, viewFrame.size.width, viewFrame.size.height)];
        }
        
    }
    [UIView commitAnimations];
    // Move your view here ...
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
    self.spinner = nil;
    [super dealloc];
}
@end
