

#import "PullRefresh_PagingViewController.h"

@implementation PullRefresh_PagingViewController
@synthesize _refreshHeaderView;
@synthesize _loadMore;
@synthesize iPageNumber;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    iPageNumber = 1;

    if (_refreshHeaderView == nil) {
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - tableView.bounds.size.height, self.view.frame.size.width, tableView.bounds.size.height)];
		view.delegate = self;
		[tableView addSubview:view];
		self._refreshHeaderView = view;
		[view release];
	}
	[_refreshHeaderView refreshLastUpdatedDate];
}

- (void)removePullToRefresh{
    [self._refreshHeaderView removeFromSuperview];
    self._refreshHeaderView = nil;
    self._refreshHeaderView.delegate = nil;
    [tableView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)reloadTableViewDataSource{
    if(!_reloading){
        [self refreshTableView];
    }
	_reloading = YES;
	
}

- (void)doneLoadingTableViewData{
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:tableView];
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
	
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//    [super scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
	if (!decelerate){
        [self loadMoreRecords];
    }
    
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
	
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
 //   [super scrollViewDidEndDecelerating:scrollView];
    [self loadMoreRecords];
}
- (void)loadMoreRecords{
    NSInteger iActualRecords = [self actualRecordCountsInTable];
    if(iActualRecords != [self desiredRecordCountsInTable]){
        NSLog(@"%d - %d",iActualRecords,[self desiredRecordCountsInTable]);
        return;
    }
    NSArray *visiblePaths = [tableView indexPathsForVisibleRows];
    for (NSIndexPath *indexPath in visiblePaths){
        NSLog(@"%d",indexPath.row);
        if(indexPath.row == iActualRecords-1){
            [tableView setTableFooterView:footerView];
            [tableView setContentOffset:CGPointMake(0.0,tableView.contentOffset.y+55) animated:YES];
            iPageNumber++;
            _loadMore = YES;
            [self loadMore];
        }
    }
}
- (NSInteger)desiredRecordCountsInTable{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        return ((iPageNumber+1)*PAGECOUNT);
    
    return ((iPageNumber+1)*PAGECOUNT_iPad);
}

- (NSInteger)actualRecordCountsInTable{
    return [self totalCount];
}
- (void)dealloc{
    [tableView release];
    [footerView release];
    self._refreshHeaderView = nil;
    [super dealloc];
}
@end
