

#import <Foundation/Foundation.h>
@protocol WebserviceDelegate;

@interface Webservice : NSObject {
	NSURLConnection *conn;
	NSMutableData *mutableData;
	id <WebserviceDelegate> _delegate;
	NSInteger iTag;
    BOOL complete;
    BOOL busy;
    long long expectedContentLength;
    BOOL responseProper;
}
@property (nonatomic, assign) id <WebserviceDelegate> _delegate;
@property (nonatomic) NSInteger iTag;
@property (nonatomic) BOOL complete;
@property (nonatomic) BOOL busy;
@property (nonatomic) BOOL responseProper;

-(void)callJSONMethod: (NSString *)methodName withParameters: (NSMutableDictionary *) params;
-(void)callJSONMethod:(NSString *)methodName withImage:(NSData*)imageData andParams:(NSMutableDictionary*)params;
-(void)callJSONGETWebserviceMethod:(NSString*)methodName withParams:(NSMutableDictionary*)dict;
-(void)callpostImage:(NSData*)imageData andParams:(NSMutableDictionary*)params extension:(NSString*)extension;
-(void)cancelWebservice;
@end

@protocol WebserviceDelegate <NSObject>
@optional
-(void)completeDownload:(id)dict withWebservice:(Webservice*)service;
-(void)dataLoadedPercentage:(CGFloat)fLoaded withWebservice:(Webservice*)service;
-(void)failWithWebService:(Webservice*)service error:(NSError*)error;
-(void)failWithWebServiceError:(NSError*)error;
@end
