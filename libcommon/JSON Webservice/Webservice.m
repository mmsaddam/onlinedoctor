
#import "Webservice.h"
#import "JSON.h"

@implementation Webservice
@synthesize _delegate;
@synthesize iTag;
@synthesize complete;
@synthesize busy;
@synthesize responseProper;

-(void)callJSONMethod:(NSString *)methodName withParameters: (NSMutableDictionary *) params
{
    complete = NO;
    busy = YES;
	NSURL *url=	[NSURL URLWithString:WEBSERVICE_URL];
	NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url]; 
	[request setHTTPMethod:@"POST"];
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    NSMutableDictionary *requestDict = [NSMutableDictionary dictionary];
    [requestDict setObject:methodName forKey:@"method_name"];
    [requestDict setObject:params forKey:@"body"];
    
	NSString *strRequest = [NSString stringWithFormat:@"json=%@",[requestDict JSONRepresentation]];
 //   strRequest = [strRequest stringByReplacingOccurrencesOfString:@"" withString:@"//u0022"];
//    strRequest = [strRequest stringByReplacingOccurrencesOfString:@"&" withString:@"//u0026"];
	NSData *data = [strRequest dataUsingEncoding:NSUTF8StringEncoding];
	[request setHTTPBody:data];

	mutableData = [[NSMutableData alloc] init];
	conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
}

-(void)callJSONMethod:(NSString *)methodName withImage:(NSData*)imageData andParams:(NSMutableDictionary*)params{
    complete = NO;
    busy = YES;
 /*   NSString *strURL = [WEBSERVICE_URL stringByAppendingFormat:@"%@",methodName];
    NSURL *url=	[NSURL URLWithString:strURL];
	NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url]; 
	[request setHTTPMethod:@"POST"];
	NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    NSArray *array = [params allKeys];
	NSInteger i= 0;
    NSString *strRequest = @"";
	for (NSString *strKey in array){
		NSString *strValue = [params objectForKey:strKey];
		if (i==0) 
			strRequest = [strRequest stringByAppendingFormat:@"%@=%@",strKey,strValue];
		else 
			strRequest = [strRequest stringByAppendingFormat:@"&%@=%@",strKey,strValue];
		i++;
	}
    
    NSData *data = [strRequest dataUsingEncoding:NSUTF8StringEncoding];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *paramsFormat = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"json"];
    [body appendData:[[NSString stringWithFormat:paramsFormat] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:data];
    [body appendData:[[NSString stringWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // file
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithString:@"Content-Disposition: attachment; name=\"pi_uploaded_image\"; filename=\"temp.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];*/
    
    NSString *strURL = [WEBSERVICE_URL stringByAppendingFormat:@"%@",methodName];
    NSArray *array = [params allKeys];
	NSInteger i= 0;
    NSString *strRequest = @"";
	for (NSString *strKey in array){
		NSString *strValue = [params objectForKey:strKey];
		if (i==0) 
			strRequest = [strRequest stringByAppendingFormat:@"?%@=%@",strKey,strValue];
		else 
			strRequest = [strRequest stringByAppendingFormat:@"&%@=%@",strKey,strValue];
		i++;
	}
    
    strURL = [strURL stringByAppendingFormat:strRequest];
    NSURL *url=	[NSURL URLWithString:strURL];
	NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
	NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    // file
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: attachment; name=\"pi_uploaded_image\"; filename=\"temp.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    mutableData = [[NSMutableData alloc] init];
	conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}
-(void)callJSONGETWebserviceMethod:(NSString*)methodName withParams:(NSMutableDictionary*)dict{
    complete = NO;
    busy = YES;
	NSString *strURL = [WEBSERVICE_URL stringByAppendingFormat:@"%@",methodName];
	NSArray *array = [dict allKeys];
	NSInteger i= 0;
	for (NSString *strKey in array){
		NSString *strValue = [dict objectForKey:strKey];
		if (i==0) 
			strURL = [strURL stringByAppendingFormat:@"?%@=%@",strKey,strValue];
		else 
			strURL = [strURL stringByAppendingFormat:@"&%@=%@",strKey,strValue];
		i++;
	}
	strURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSLog(@"%@",strURL);
	
	mutableData = [[NSMutableData data] retain];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strURL]];
	conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (NSMutableData*)generatePostBody:(NSMutableDictionary *)_params bodyLength:(int *)length {
    
    NSString *BOUNDARY = @"---------------------------7d92ff162077c";
    NSMutableData* body = [NSMutableData data];
    NSString* endLine = [NSString stringWithFormat:@"\r\n--%@\r\n", BOUNDARY];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BOUNDARY] dataUsingEncoding:NSUTF8StringEncoding]];
    
    for (id key in [_params keyEnumerator]) {
        if (![[_params objectForKey:key] isKindOfClass:[NSData class]]) {
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[_params valueForKey:key] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[endLine dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    for (id key in [_params keyEnumerator]) {        
        if ([[_params objectForKey:key] isKindOfClass:[NSData class]]) {
            NSData *textData = [_params objectForKey:key];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"media_data\"; filename=\"%@\"\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:textData];
            continue;
            
        }
        
    }
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", BOUNDARY] dataUsingEncoding:NSUTF8StringEncoding]];
    *length = [body length];
    return body;
    
}

-(void)callpostImage:(NSData*)imageData andParams:(NSMutableDictionary*)params extension:(NSString*)extension{
    complete = NO;
    busy = YES;
    NSString *strURL = [WEBSERVICE_URL stringByAppendingFormat:@"%@",extension];
    NSURL *url=	[NSURL URLWithString:strURL];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url]; 
    [request setHTTPMethod:@"POST"];
    NSString *BOUNDARY = @"---------------------------7d92ff162077c";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BOUNDARY];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSInteger itemp;
    [params setObject:imageData forKey:@"testimage"];
    NSMutableData *body = [self generatePostBody:params bodyLength:&itemp];
    [request addValue:[NSString stringWithFormat:@"%d", itemp] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:body];
    mutableData = [[NSMutableData alloc] init];
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}
-(void)cancelWebservice{
    busy = NO;
    if(mutableData){
        [mutableData release];
        mutableData = nil;
    }
    if(conn){
        [conn cancel];
        [conn release];
        conn = nil;
    }
}

#pragma mark -
#pragma mark Download support (NSURLConnectionDelegate)

- (void)connection:(NSURLConnection *)conection didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse *httpResponse;
	
	httpResponse = (NSHTTPURLResponse *)response;
	
	assert([httpResponse isKindOfClass:[NSHTTPURLResponse class]]);
	
	if ((httpResponse.statusCode / 100) != 2) {
		responseProper = NO;
	} 
	else {
         expectedContentLength = response.expectedContentLength;
            responseProper = YES;
			[mutableData setLength:0];
	}
   
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [mutableData appendData:data];
    if ([_delegate respondsToSelector:@selector(dataLoadedPercentage:withWebservice:)]) {
        double dPercentage = (mutableData.length*100)/expectedContentLength;
		[_delegate dataLoadedPercentage:dPercentage withWebservice:self];
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    complete = YES;
     busy = NO;
    if(mutableData){
		[mutableData release];
		mutableData = nil;
	}
	if(conn){
		[conn release];
		conn = nil;
	}
	
	if ([_delegate respondsToSelector:@selector(failWithWebServiceError:)]) {
		[_delegate failWithWebServiceError:error];
	}
    else if([_delegate respondsToSelector:@selector(failWithWebService:error:)]){
        [_delegate failWithWebService:self error:error];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    complete = YES;
    busy = NO;
	NSString* returnString = [[[NSString alloc] initWithData:mutableData encoding:NSUTF8StringEncoding] autorelease];
    returnString = [returnString stringByReplacingOccurrencesOfString:@"__u0022__" withString:@"''"];
    returnString = [returnString stringByReplacingOccurrencesOfString:@"__u0026__" withString:@"&"];
    returnString = [returnString stringByReplacingOccurrencesOfString:@"__u000a__" withString:@"\\n"];
	id dict = [returnString JSONValue];

	if([dict isKindOfClass:[NSArray class]] || [dict isKindOfClass:[NSMutableArray class]]){
        NSArray *array = (NSArray*)dict;
        NSMutableDictionary *dict = [array objectAtIndex:0];
        NSString *status = [dict objectForKey:@"status"];
        if([status intValue] == 101){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"session_expire" object:nil];
            return;
        }
    }
	if ([_delegate respondsToSelector:@selector(completeDownload:withWebservice:)]) {
		[_delegate completeDownload:dict withWebservice:self];
	}
	
	if(mutableData){
		[mutableData release];
		mutableData = nil;
	}
	if(conn){
		[conn release];
		conn = nil;
	}
	
}

-(void)dealloc{
    if(mutableData){
		[mutableData release];
		mutableData = nil;
	}
	if(conn){
		[conn release];
		conn = nil;
	}
    [super dealloc];
}



@end
