//
//  ParentViewController.h
//  Restaurant
//
//  Created by Darshit on 16/10/13.
//  Copyright (c) 2013 TriState Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentViewController : UIViewController
@property (assign, nonatomic) BOOL isStatusBarSet;

-(BOOL)isiOS7;
-(BOOL)isForIphone4;
-(BOOL)isForIpad;

@end
