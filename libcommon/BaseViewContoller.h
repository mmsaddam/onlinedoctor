

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "ParentViewController.h"

@interface BaseViewContoller : ParentViewController{
    MBProgressHUD *HUD;
    AppDelegate *appDelegate;
    IBOutlet UIActivityIndicatorView *spinner;
    CGRect viewFrame;
    CGFloat intialStatuHeight;
}
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *spinner;
- (BOOL)isNetworkReachable:(NSString*)NetPath;
- (BOOL)isNetworkReachable;
- (void)showHud;
- (void)showHudWithText:(NSString*)txt;
- (void)hidHud;
- (void)showSpinnerWithUserActionEnable:(BOOL)isEnable;
- (void)hideSpinner;
@end
