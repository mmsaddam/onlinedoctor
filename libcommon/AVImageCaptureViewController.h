

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AVImageCaptureViewController : UIViewController <UIGestureRecognizerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate>{
    
    IBOutlet UIView *previewView;
    IBOutlet UISegmentedControl *camerasControl;
    AVCaptureVideoPreviewLayer *previewLayer;
	AVCaptureVideoDataOutput *videoDataOutput;
    dispatch_queue_t videoDataOutputQueue;
	AVCaptureStillImageOutput *stillImageOutput;
	UIView *flashView;
    BOOL isUsingFrontFacingCamera;
    CGFloat beginGestureScale;
	CGFloat effectiveScale;
}

- (IBAction)takePicture:(id)sender;
- (IBAction)switchCameras:(id)sender;
- (IBAction)handlePinchGesture:(UIGestureRecognizer *)sender;

- (void)setupAVCapture;
- (void)teardownAVCapture;
- (void)autorotateToInterfaceOrientation;
-(void)capturedImage:(UIImage *)image Tag:(int)tag;
-(IBAction)flashLightBtnAction:(id)sender;

@end
