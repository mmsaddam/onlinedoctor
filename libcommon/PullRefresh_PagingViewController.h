

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "BaseViewContoller.h"

@interface PullRefresh_PagingViewController : BaseViewContoller <EGORefreshTableHeaderDelegate>{
    IBOutlet UIView *footerView;
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
    NSInteger iPageNumber;
    BOOL _loadMore;
    IBOutlet UITableView *tableView;
}
@property (nonatomic, retain) EGORefreshTableHeaderView *_refreshHeaderView;
@property (nonatomic) BOOL _loadMore;
@property (nonatomic)NSInteger iPageNumber;
-(void)refreshTableView;
- (NSInteger)desiredRecordCountsInTable;
- (NSInteger)actualRecordCountsInTable;
- (void)loadMoreRecords;
- (NSInteger)totalCount;
- (void)loadMore;
- (void)removePullToRefresh;

- (void)doneLoadingTableViewData;
- (void)reloadTableViewDataSource;
@end
